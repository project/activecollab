
if (Drupal.jsEnabled) {
  $(document).ready(function() {
    
  });
}

function activeCollabCall(call, callback, post) {
  var url = Drupal.settings.activeCollab.url;
  if (post) {
    call = call + post;
  }
  $.ajax({
    url: url,
    type: "POST",
    dataType: 'json',
    data: "call=" + call,
    success: function(data) {
      return callback(data)
    }
  });
}
