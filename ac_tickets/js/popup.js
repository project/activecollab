
if (Drupal.jsEnabled) {
  $(document).ready(function() {
    var activeCollab;
    if (activeCollab = Drupal.settings.activeCollab) {
      var nameID = activeCollab.tickets.popup.project;

      if (nameID) {
        var project = nameID.split('-');
        var name = project[0];
        var id = project[1];

        $("#ac-tickets-title a").click(function() {
          if ($("#ac-tickets-popup").css('height') == '20px') {
            $("#ac-tickets-popup").animate({
              height: 360,
              width: 675
            }, 'fast');
          } else {
            $("#ac-tickets-popup").animate({
              height: 20,
              width: 100
            }, 'fast');
          }
          return false;
        });

        $("#ac-tickets-menu-create").click(function() {
          if ($("#ac-tickets-create").css('display') == 'none') {
            $(this).toggleClass('selected');
            if ($("#ac-tickets-list").css('display') != 'none') {
              $("#ac-tickets-menu-list").toggleClass('selected');
            }

            $("#ac-tickets-create").show();
            $("#ac-tickets-list").hide();
            $("#ac-tickets-view").hide();
          }
          return false;
        });

        $("#ac-tickets-menu-list").click(function() {
          activeCollabCall('/projects/' + id + '/tickets', function(ticketsResult) {
            var result = activeCollabTickets(ticketsResult);
            $("#ac-tickets-list").empty().append(result);
          });

          if ($("#ac-tickets-list").css('display') == 'none') {
            if ($("#ac-tickets-create").css('display') != 'none') {
              $("#ac-tickets-menu-create").toggleClass('selected');
            }
            $(this).toggleClass('selected');

            $("#ac-tickets-create").hide();
            $("#ac-tickets-list").show();
            $("#ac-tickets-view").hide();
          }
          return false;
        });

        $("#ac-tickets-create #edit-submit").click(function() {
          var errors = new Array();
          if ($("#ac-tickets-create #edit-summary").val() == '') {
            errors.push('Summary is required');
          }
          if ($("#ac-tickets-create #edit-body").val() == '') {
            errors.push('Full Description is required');
          }
          if (errors.length) {
            for (var i in errors) {
              alert(errors[i]);
            }
          } else {
            var summary = $("#ac-tickets-create #edit-summary").val();
            var body = $("#ac-tickets-create #edit-body").val();
            var milestone = $("#ac-tickets-create #edit-milestone").val();
            var tags = $("#ac-tickets-create #edit-tags").val();
            var priority = $("#ac-tickets-create #edit-priority").val();
            var ticket = '&type=ticket&name=' + summary + '&body=' + body;
            if (milestone != 0) {
              ticket+= '&milestone_id=' + milestone;
            }
            if (tags != 0) {
              ticket+= '&tags=' + tags;
            }
            if ($("#ac-tickets-create #edit-visibility-1-wrapper input").val() == 1) {
              ticket+= '&visibility=1';
            } else {
              ticket+= '&visibility=0';
            }
            ticket+= '&priority=' + priority;

            activeCollabCall('/projects/' + id + '/tickets/add', function(ticketsResult) {
              activeCollabTicketLoad(ticketsResult);
            }, ticket);
          }
          return false;
        });

        // Active list tab if perm is set to 'view'
        if (activeCollab.tickets.perm == 'view') {
          $("#ac-tickets-menu-list").trigger('click');
        }
      }
    }
  });
}

function activeCollabTickets(ticketsResult) {
  var tickets = new Array();
  for (var i in ticketsResult) {
    var ticket = ticketsResult[i];
    var priority = ticket.priority;
    if (ticket.milestone) {
      if (!tickets[ticket.milestone.id]) {
        tickets[ticket.milestone.id] = '<div id="ac-tickets-milestone-' + ticket.milestone.id + '">';
        tickets[ticket.milestone.id]+= '<h2><span>' + ticket.milestone.name + '</span></h2><div class="ac-tickets-milestone-content">';
      }
      tickets[ticket.milestone.id]+= '<div class="ac-ticket-priority-' + ticket.priority + '"><span>#' + ticket.ticket_id + '</span><a href="" onclick="activeCollabTicketLoad(' + ticket.project_id + ',' + ticket.ticket_id + '); return false;">' + ticket.name + '</a></div>';
    } else {
      if (!tickets[ticket.milestone_id]) {
        tickets[ticket.milestone_id] = '<div id="ac-tickets-milestone-unknown">';
        tickets[ticket.milestone_id]+= '<h2><span>Unknown Milestone</span></h2><div class="ac-tickets-milestone-content">';
      }
      tickets[ticket.milestone_id]+= '<div class="ac-ticket-priority-' + ticket.priority + '"><span>#' + ticket.ticket_id + '</span><a href="" onclick="activeCollabTicketLoad(' + ticket.project_id + ',' + ticket.ticket_id + '); return false;">' + ticket.name + '</a></div>';
    }
  }

  var milestones = '';
  for (var i in tickets) {
    milestone = tickets[i];
    milestone+= '</div></div>';
    milestones+= milestone;
  }
  return milestones;
}

function activeCollabTicketLoad(project, id) {
  if (typeof project == 'object') {
    var ticket = project;
    var output = '<h4>Ticket #' + ticket.ticket_id + ": " + ticket.name + '</h4>';
    output+= '<p>' + ticket.body + '</p>';
    $("#ac-tickets-view").empty().append(output);
    $("#ac-tickets-menu-create").toggleClass('selected');
    $("#ac-tickets-create").hide();
    $("#ac-tickets-view").show();
  } else {
    activeCollabCall('/projects/' + project + '/tickets/' + id, function(ticket) {
      var output = '<h4>Ticket #' + id + ": " + ticket.name + '</h4>';
      output+= '<p>' + ticket.body + '</p>';
      $("#ac-tickets-view").empty().append(output);
      $("#ac-tickets-menu-list").toggleClass('selected');
      $("#ac-tickets-list").hide();
      $("#ac-tickets-view").show();
    });
  }
}
